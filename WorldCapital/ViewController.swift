//
//  ViewController.swift
//  WorldCapital
//
//  Created by esersahin on 30/12/2016.
//  Copyright © 2016 esersahin. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

  @IBOutlet weak var image1: UIImageView!
  
  @IBOutlet weak var image2: UIImageView!
  
  @IBOutlet weak var label1: UILabel!
  @IBOutlet weak var label2: UILabel!
  @IBOutlet weak var message: UILabel!
  
  @IBOutlet weak var picker: UIPickerView!
  
  
  var pickerData: [[String]] = [[String]]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    self.picker.delegate = self
    self.picker.dataSource = self
    
    pickerData = [["USA", "Italy", "China", "England"],
                  ["Beijing", "London", "Rome", "Washington, DC"],
                  //,["Izmir", "Istanbul", "Ankara", "Antalya"]
                 ]
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

 
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return pickerData.count
  }
  
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return pickerData[component].count
  }
  
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return pickerData[component][row]
  }
}

